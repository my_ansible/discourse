---
- debug:
    msg: "Provisioning a Let's Encrypt cert for {{ common_name }} with the following subjectAltNames {{ subject_alt_names }}"
    verbosity: 1

- name: "Stat /root/.acme.sh/{{ common_name }}_ecc/{{ common_name }}.cer"
  stat:
    path: "/root/.acme.sh/{{ common_name }}_ecc/{{ common_name }}.cer"
  register: le_cert_exists

- debug:
    msg: "The results of /root/.acme.sh/{{ common_name }}_ecc/{{ common_name }}.cer : {{ le_cert_exists.stat.exists }}"
    verbosity: 1

- name: "Stat {{ le_dir }}/{{ common_name }}.cert.pem"
  stat:
    path: "{{ le_dir }}/{{ common_name }}.cert.pem"
  register: le_cert_installed

- debug:
    msg: "The result of stat.exists {{ le_dir }}/{{ common_name }}.cert/pem : {{ le_cert_installed.stat.exists }}"
    verbosity: 1

#- set_fact:
#     le_enabled: "{{ le_cert_installed.stat.exists }}"
#
#- debug:
#    msg: "The value of le_enabled : {{ le_enabled }}"
#    verbosity: 1

# CommonsCloud. hostname -i returns "127.0.0.1" and hostname -I returns Scaleway's private server address
# the playbook expects it to be the ip of {{ hostname }} so therefore checks against hostname_ip it fail
# command: hostname -i
# So I changed it to "dig {{ hostname }} +short | tail -1"
# Could this be fixed with inventory?
- name: Check the server IP address
  shell: "dig {{ hostname }} +short | tail -1"
  register: hostname_ip

- debug:
    msg: "The results of `hostname -i`: {{ hostname_ip.stdout }}"
    verbosity: 1

- debug:
    msg: "The subject_alt_names : {{ subject_alt_names }}"
    verbosity: 1

- name: Include Let's Encrypt subjectAltNames 
  include_tasks: subject_alt_names.yml
  with_items: 
    - "{{ subject_alt_names }}"

- debug:
    msg: "The list of domains for acme.sh: {{ le_domains }}"
    verbosity: 1

- debug:
    msg: "The list of domains to be checked : {{ dns_subject_alt_names }}"
    verbosity: 1

- debug:
    msg: "The value of le_cert_exists.stat.exists : {{ le_cert_exists.stat.exists }}"
    verbosity: 1

- debug:
    msg: "The value of regenerate_cert : {{ regenerate_cert }}"
    verbosity: 1
  when: regenerate_cert is defined

- block:

  - debug:
      msg: "Let's Encrypt command to be run: /root/.acme.sh/acme.sh --issue {{ le_domains }} -w /var/www --keylength ec-256"
      verbosity: 1

  - name: "Provision Let's Encrypt ECC cert for {{ common_name }}" 
    command: "/root/.acme.sh/acme.sh --issue {{ le_domains }} -w /var/www --keylength ec-256"
    register: new_le_cert

  when: ( le_cert_exists.stat.exists == False ) or ( regenerate_cert is defined and regenerate_cert == True )

- name: "Stat /root/.acme.sh/{{ common_name }}_ecc/{{ common_name }}.cer"
  stat:
    path: "/root/.acme.sh/{{ common_name }}_ecc/{{ common_name }}.cer"
  register: le_cert_exists

- debug:
    msg: "The results of /root/.acme.sh/{{ common_name }}_ecc/{{ common_name }}.cer : {{ le_cert_exists.stat.exists }}"
    verbosity: 1

- debug:
    msg: "Let's Encrypt command to be run: /root/.acme.sh/acme.sh --installcert --ecc -d {{ common_name }} --fullchainpath {{ le_dir }}/{{ common_name }}.fullchain.pem --certpath {{ le_dir }}/{{ common_name }}.cert.pem --keypath {{ le_dir }}/{{ common_name }}.key.pem --capath {{ le_dir }}/{{ common_name }}.ca.pem --reloadcmd 'service apache2 reload'"
    verbosity: 1

- name: "Let's Encrypt ECC cert for {{ common_name }} in place"
  command: "/root/.acme.sh/acme.sh --installcert --ecc -d {{ common_name }} --fullchainpath {{ le_dir }}/{{ common_name }}.fullchain.pem --certpath {{ le_dir }}/{{ common_name }}.cert.pem --keypath {{ le_dir }}/{{ common_name }}.key.pem --capath {{ le_dir }}/{{ common_name }}.ca.pem --reloadcmd 'service apache2 reload'"
  when: le_cert_exists.stat.exists == True

- name: "Stat {{ le_dir }}/{{ common_name }}.key.pem"
  stat:
    path: "{{ le_dir }}/{{ common_name }}.key.pem"
  register: le_key_installed

- debug:
    msg: "The results of le_key_installed.stat.exists {{ le_key_installed.stat.exists }}"
    verbosity: 1

- name: Let's Encrypt key only readable by root and ssl-cert
  file:
    path: "{{ le_dir }}/{{ common_name }}.key.pem"
    mode: 0640
    owner: root
    group: ssl-cert
  when: le_key_installed.stat.exists

- name: "Stat {{ le_dir }}/{{ common_name }}.cert.pem"
  stat:
    path: "{{ le_dir }}/{{ common_name }}.cert.pem"
  register: le_cert_installed

- debug:
    msg: "The results of le_cert_installed.stat.exists {{ le_cert_installed.stat.exists }}"
    verbosity: 1

- set_fact:
     le_enabled: True
  when: ( le_key_installed.stat.exists == True ) and ( le_cert_installed.stat.exists == True )

- debug:
    msg: "The variable le_enabled is set to : {{ le_enabled }}"
    verbosity: 1

- name: Cert check crontab in place
  cron:
    name: Cert check
    minute: 45
    hour: 8
    job: "ssl-cert-check -qac {{ le_dir }}/{{ common_name }}.cert.pem -e 'root@localhost'"
  when: ( le_enabled is defined ) and ( le_enabled == True )
