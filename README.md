Ansible playbooks for [Discourse](https://www.discourse.org/) on Debian Stretch, the initial import of files in this repo was copied from the [CoTech Ansible](https://git.coop/cotech/ansible) repo.

This repo uses the [Webarchitects Docker Ansible role](https://git.coop/webarch/docker) to install Docker CE.

The email setup is based on the [mail-reciever Docker container](https://github.com/discourse/mail-receiver) plus [this pull request](https://github.com/discourse/mail-receiver/pull/2) (which is now merged) and the [Postfix notes for using the host for outgoing email](https://meta.discourse.org/t/emails-with-local-smtp/23645/28), with an additional [Ruby script](https://git.coop/webarch/discourse/blob/master/roles/email/files/discourse-smtp-rcpt-acl).


## Install 

Login to the console of the virtual server and install python, `apt install python` and make a `/root/.ssh` directory and add your ssh public keys to a `/root/.ssh/authorized_keys` file, then:

```bash
export SERVERNAME="discourse.webarchitects.co.uk"
ansible-galaxy install -r requirements.yml --force -p roles 
ansible-playbook -u root discourse.yml -i "${SERVERNAME}," -e "hostname=${SERVERNAME}"
```

The prompt *"Do you want to map server and container User IDs and Group IDs?"* can be left at the default value, *True* and only changed to *False* if the task fails &mdash; it will fail if packages other than minimal Debian packages have been installed on the server.

Then login to the site, get the API key from https://$SERVERNAME/admin/api/keys and run the second Playbook, adding the API key when prompted:

```bash
export SERVERNAME="discourse.webarchitects.co.uk"
ansible-playbook -u root discourse_api.yml -i "${SERVERNAME}," -e "hostname=${SERVERNAME}"
```

**Note:** If you  install a backup from another server you will need to re-run the API Playbook to update the server before email will work. 

Then check these settings for email:

* **Required : notification email** set this to `discourse@$SERVERNAME` (use the actual domain name not $SERVERNAME)
* **Email : reply by email enabled** tick *"Enable replying to topics via email."*
* **Email : reply by email address** set this to `discourse+%{reply_key}@$SERVERNAME` (use the actual domain name not $SERVERNAME)
* **Email : manual polling enabled** tick *"Push emails using the API for email replies."*

Then tighten some security settings:

* **Security : force https** tick *"Force your site to use HTTPS only. WARNING: do NOT enable this until you verify HTTPS is fully set up and working absolutely everywhere! Did you check your CDN, all social logins, and any external logos / dependencies to make sure they are all HTTPS compatible, too?"*

If you are using this Playbook somewhere other than on a [Webarchitects](https://www.webarchitects.coop/) virtual server in Sheffield then the `iptables` and `munin-node` roles will, as a minimum, need editing and might be best omitted. Also note that these Playbooks are based on using `mx.webarch.net` for incoming email -- this is an anti-spam gateway, if this wasn't used then SpamAssassin should probably be added to the mix.


## Discourse Upgrade

To upgrade Discourse you can use this Playbook:

```bash
export SERVERNAME="discourse.webarchitects.co.uk"
ansible-playbook -u root discourse_upgrade.yml -i "${SERVERNAME}," -e "hostname=${SERVERNAME}"
```
